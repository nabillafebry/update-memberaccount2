package id.co.asyst.amala.memberaccount.detail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"memberaccount-detail.xml", "beans.xml"})
public class MemberAccountDetailApp {

    public static void main(String[] args) {
        SpringApplication.run(id.co.asyst.amala.memberaccount.detail.MemberAccountDetailApp.class, args);
    }
}