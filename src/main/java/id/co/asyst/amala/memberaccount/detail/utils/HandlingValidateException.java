package id.co.asyst.amala.memberaccount.detail.utils;

public class HandlingValidateException extends Exception {

    public HandlingValidateException(String message) {
        super(message);
    }
}
