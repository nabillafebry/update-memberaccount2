package id.co.asyst.amala.memberaccount.detail.utils;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Utils {
    static Logger LOG = LoggerFactory.getLogger(Utils.class);

    List<Map<String, Object>> listMember = new ArrayList<>();


    public void readFile(Exchange exchange) {
        String filepath = "/filestore/file-integration/migration/csvmember/";
        String filename = "r_a_02.csv";
//        String filename = "E:/Book1.csv";
        List listData = new ArrayList();
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        int count = 0;

        try {

            br = new BufferedReader(new FileReader(filepath + filename));
            while ((line = br.readLine()) != null) {

                Map<String, String> mapData = new HashMap<>();
                String[] data = line.split(cvsSplitBy);

                count += 1;

                mapData.put("count", String.valueOf(count));
                mapData.put("certificateid", data[1]);
                mapData.put("detailid", data[2]);
                mapData.put("miles", data[3]);

                listData.add(mapData);
            }

            exchange.setProperty("listData", listData);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
