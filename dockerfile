# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/update-memberaccount2.jar /update-memberaccount2.jar
# run application with this command line[
CMD ["java", "-jar", "/update-memberaccount2.jar"]
